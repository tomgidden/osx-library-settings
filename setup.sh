#!/usr/bin/env bash

defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to black - runWorkflowAsService"' '{key_equivalent = "^~@0";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to red - runWorkflowAsService"' '{key_equivalent = "^~@1";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to yellow - runWorkflowAsService"' '{key_equivalent = "^~@3";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to green - runWorkflowAsService"' '{key_equivalent = "^~@2";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to blue - runWorkflowAsService"' '{key_equivalent = "^~@4";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to cyan - runWorkflowAsService"' '{key_equivalent = "^~@6";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to magenta - runWorkflowAsService"' '{key_equivalent = "^~@5";}'
defaults write pbs NSServicesStatus -dict-add '"(null) - Change terminal to grey - runWorkflowAsService"' '{key_equivalent = "^~@7";}'
